<?php

namespace Webjump\HelloWorld\Observer\FrontEnd;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Psr\Log\LoggerInterface as PsrLoggerInterface;

/**
 * Class PredispatchObserver
 */
class PredispatchObserver extends AbstractDataAssignObserver
{
    /**
     * @var PsrLoggerInterface
     */
    private $logger;

    /**
     * PredispatchObserver constructor.
     *
     * @param PsrLoggerInterface $logger
     */
    public function __construct(
        PsrLoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $message = "PredispatchObserver";
        $this->logger->debug($message);
    }
}
