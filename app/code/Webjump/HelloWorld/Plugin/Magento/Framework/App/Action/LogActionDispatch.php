<?php

namespace Webjump\HelloWorld\Plugin\Magento\Framework\App\Action;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface as PsrLoggerInterface;
use Exception;

/**
 * Class LogActionDispatch
 */
class LogActionDispatch
{
    /**
     * @var PsrLoggerInterface
     */
    private $logger;

    /**
     * LogActionDispatch constructor.
     *
     * @param PsrLoggerInterface $logger
     */
    public function __construct(
        PsrLoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * Log ActionDispatch
     *
     * @param Action $subject
     * @param $result
     * @return array
     */
    public function afterDispatch(Action $subject, $result)
    {
        $message = "after ActionDispatch";
        $this->logger->debug($message);

        return $result;
    }

    /**
     * Log ActionDispatch
     *
     * @param Action $subject
     * @param RequestInterface $request
     */
    public function beforeDispatch(Action $subject, RequestInterface $request) {
        $message = "before ActionDispatch";
        $this->logger->debug($message);
    }

    public function aroundDispatch(Action $subject, callable $proceed, ...$args)
    {
        $message = "around ActionDispatch";
        $this->logger->critical($message);
        return $proceed(...$args);
    }
}
