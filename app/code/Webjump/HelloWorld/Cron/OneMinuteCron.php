<?php

namespace Webjump\HelloWorld\Cron;

use Psr\Log\LoggerInterface as PsrLoggerInterface;

class OneMinuteCron
{
    /**
     * @var PsrLoggerInterface
     */
    private $logger;

    /**
     * OneMinuteCron constructor.
     *
     * @param PsrLoggerInterface $logger
     */
    public function __construct(
        PsrLoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function execute()
    {
        $message = "OneMinuteCron";
        $this->logger->debug($message);
    }
}
