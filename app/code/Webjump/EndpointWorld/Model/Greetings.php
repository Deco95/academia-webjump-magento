<?php
declare(strict_types=1);

namespace Webjump\EndpointWorld\Model;

use Webjump\EndpointWorld\Api\GreetingsInterface;

class Greetings implements GreetingsInterface
{
    /**
     * @param string|null $name
     * @return string[]
     */
    public function hello(?string $name = 'Strange'): array
    {
        if (empty($name)){
            $name = 'Strange';
        }
        $response['success'] =  'true';
        $response['message'] = 'Hello ' . $name;
        return [$response];
    }
}
