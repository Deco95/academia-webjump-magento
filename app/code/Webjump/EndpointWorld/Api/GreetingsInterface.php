<?php
declare(strict_types=1);

namespace Webjump\EndpointWorld\Api;

/**
 * Api Interface GreetingsInterface
 */
interface GreetingsInterface
{
    /**
     * Returns hello name, with name as the given parameter
     *
     * @param string|null $name
     * @return string[]
     */
    public function hello(?string $name = 'Strange'): array;
}
