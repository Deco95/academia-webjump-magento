<?php
declare(strict_types=1);

namespace Webjump\Pet\Controller\Adminhtml\PetKind;

class Delete extends \Webjump\Pet\Controller\Adminhtml\PetKind
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('petkind_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Webjump\Pet\Model\PetKind::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Pet Kind.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['petkind_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Pet Kind to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

