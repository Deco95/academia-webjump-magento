<?php
declare(strict_types=1);

namespace Webjump\Pet\Controller\Adminhtml\PetKind;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            return $resultRedirect->setPath('*/*/');
        }

        $requestEntityId = $this->getRequest()->getParam('petkind_id');

        $model = $this->_objectManager->create(\Webjump\Pet\Model\PetKind::class)->load($requestEntityId);
        if (!$model->getId() && $requestEntityId) {
            $this->messageManager->addErrorMessage(__('This Pet Kind no longer exists.'));
            return $resultRedirect->setPath('*/*/');
        }

        $model->setData($data);

        try {
            $model->save();
            $this->messageManager->addSuccessMessage(__('You saved the Pet Kind.'));
            $this->dataPersistor->clear('webjump_pet_petkind');

            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['petkind_id' => $model->getId()]);
            }
            return $resultRedirect->setPath('*/*/');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Pet Kind.'));
        }

        $this->dataPersistor->set('webjump_pet_petkind', $data);
        return $resultRedirect->setPath('*/*/edit', ['petkind_id' => $requestEntityId]);
    }
}

