<?php
declare(strict_types=1);

namespace Webjump\Pet\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Webjump\Pet\Api\Data\PetKindInterfaceFactory;
use Webjump\Pet\Api\Data\PetKindSearchResultsInterfaceFactory;
use Webjump\Pet\Api\PetKindRepositoryInterface;
use Webjump\Pet\Model\ResourceModel\PetKind as ResourcePetKind;
use Webjump\Pet\Model\ResourceModel\PetKind\CollectionFactory as PetKindCollectionFactory;

class PetKindRepository implements PetKindRepositoryInterface
{

    protected $resource;

    protected $petKindFactory;

    protected $petKindCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataPetKindFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourcePetKind $resource
     * @param PetKindFactory $petKindFactory
     * @param PetKindInterfaceFactory $dataPetKindFactory
     * @param PetKindCollectionFactory $petKindCollectionFactory
     * @param PetKindSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePetKind $resource,
        PetKindFactory $petKindFactory,
        PetKindInterfaceFactory $dataPetKindFactory,
        PetKindCollectionFactory $petKindCollectionFactory,
        PetKindSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->petKindFactory = $petKindFactory;
        $this->petKindCollectionFactory = $petKindCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPetKindFactory = $dataPetKindFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Webjump\Pet\Api\Data\PetKindInterface $petKind
    ) {
        /* if (empty($petKind->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $petKind->setStoreId($storeId);
        } */

        $petKindData = $this->extensibleDataObjectConverter->toNestedArray(
            $petKind,
            [],
            \Webjump\Pet\Api\Data\PetKindInterface::class
        );

        $petKindModel = $this->petKindFactory->create()->setData($petKindData);

        try {
            $this->resource->save($petKindModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Pet Kind: %1',
                $exception->getMessage()
            ));
        }
        return $petKindModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($petKindId)
    {
        $petKind = $this->petKindFactory->create();
        $this->resource->load($petKind, $petKindId);
        if (!$petKind->getId()) {
            throw new NoSuchEntityException(__('Pet Kind with id "%1" does not exist.', $petKindId));
        }
        return $petKind->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->petKindCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Webjump\Pet\Api\Data\PetKindInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Webjump\Pet\Api\Data\PetKindInterface $petKind
    ) {
        try {
            $petKindModel = $this->petKindFactory->create();
            $this->resource->load($petKindModel, $petKind->getPetkindId());
            $this->resource->delete($petKindModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Pet Kind: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($petKindId)
    {
        return $this->delete($this->get($petKindId));
    }
}

