<?php
declare(strict_types=1);

namespace Webjump\Pet\Model\Data;

use Webjump\Pet\Api\Data\PetKindInterface;

class PetKind extends \Magento\Framework\Api\AbstractExtensibleObject implements PetKindInterface
{

    /**
     * Get petkind_id
     * @return string|null
     */
    public function getPetkindId()
    {
        return $this->_get(self::PETKIND_ID);
    }

    /**
     * Set petkind_id
     * @param string $petkindId
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setPetkindId($petkindId)
    {
        return $this->setData(self::PETKIND_ID, $petkindId);
    }

    /**
     * Get Name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set Name
     * @param string $name
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Description
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * Set Description
     * @param string $description
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Webjump\Pet\Api\Data\PetKindExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Webjump\Pet\Api\Data\PetKindExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Webjump\Pet\Api\Data\PetKindExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

