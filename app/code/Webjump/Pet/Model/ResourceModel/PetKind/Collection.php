<?php
declare(strict_types=1);

namespace Webjump\Pet\Model\ResourceModel\PetKind;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'petkind_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webjump\Pet\Model\PetKind::class,
            \Webjump\Pet\Model\ResourceModel\PetKind::class
        );
    }
}

