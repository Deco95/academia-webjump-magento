<?php
declare(strict_types=1);

namespace Webjump\Pet\Model\ResourceModel;

class PetKind extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('webjump_pet_petkind', 'petkind_id');
    }
}

