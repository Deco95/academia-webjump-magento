<?php
declare(strict_types=1);

namespace Webjump\Pet\Model;

use Magento\Framework\Api\DataObjectHelper;
use Webjump\Pet\Api\Data\PetKindInterface;
use Webjump\Pet\Api\Data\PetKindInterfaceFactory;

class PetKind extends \Magento\Framework\Model\AbstractModel
{

    protected $petkindDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'webjump_pet_petkind';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PetKindInterfaceFactory $petkindDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Webjump\Pet\Model\ResourceModel\PetKind $resource
     * @param \Webjump\Pet\Model\ResourceModel\PetKind\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PetKindInterfaceFactory $petkindDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Webjump\Pet\Model\ResourceModel\PetKind $resource,
        \Webjump\Pet\Model\ResourceModel\PetKind\Collection $resourceCollection,
        array $data = []
    ) {
        $this->petkindDataFactory = $petkindDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_init(\Webjump\Pet\Model\ResourceModel\PetKind::class);
    }

    /**
     * Retrieve petkind model with petkind data
     * @return PetKindInterface
     */
    public function getDataModel()
    {
        $petkindData = $this->getData();

        $petkindDataObject = $this->petkindDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $petkindDataObject,
            $petkindData,
            PetKindInterface::class
        );

        return $petkindDataObject;
    }
}

