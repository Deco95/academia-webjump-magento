<?php
declare(strict_types=1);

namespace Webjump\Pet\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PetKindRepositoryInterface
{

    /**
     * Save PetKind
     * @param \Webjump\Pet\Api\Data\PetKindInterface $petKind
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Webjump\Pet\Api\Data\PetKindInterface $petKind
    );

    /**
     * Retrieve PetKind
     * @param string $petkindId
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($petkindId);

    /**
     * Retrieve PetKind matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Webjump\Pet\Api\Data\PetKindSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PetKind
     * @param \Webjump\Pet\Api\Data\PetKindInterface $petKind
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Webjump\Pet\Api\Data\PetKindInterface $petKind
    );

    /**
     * Delete PetKind by ID
     * @param string $petkindId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($petkindId);
}

