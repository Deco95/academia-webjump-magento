<?php
declare(strict_types=1);

namespace Webjump\Pet\Api\Data;

interface PetKindSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PetKind list.
     * @return \Webjump\Pet\Api\Data\PetKindInterface[]
     */
    public function getItems();

    /**
     * Set PetKind list.
     * @param \Webjump\Pet\Api\Data\PetKindInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

