<?php
declare(strict_types=1);

namespace Webjump\Pet\Api\Data;

interface PetKindInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PETKIND_ID = 'petkind_id';

    /**
     * Get petkind_id
     * @return string|null
     */
    public function getPetkindId();

    /**
     * Set petkind_id
     * @param string $petkindId
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setPetkindId($petkindId);

    /**
     * Get Name
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     * @param string $name
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setName($name);

    /**
     * Get Description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set Description
     * @param string $description
     * @return \Webjump\Pet\Api\Data\PetKindInterface
     */
    public function setDescription($description);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Webjump\Pet\Api\Data\PetKindExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Webjump\Pet\Api\Data\PetKindExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Webjump\Pet\Api\Data\PetKindExtensionInterface $extensionAttributes
    );
}

